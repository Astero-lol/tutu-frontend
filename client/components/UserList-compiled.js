'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _Search = require('./Search');

var _Search2 = _interopRequireDefault(_Search);

var _SortItem = require('./SortItem');

var _SortItem2 = _interopRequireDefault(_SortItem);

var _SelectedUser = require('./SelectedUser');

var _SelectedUser2 = _interopRequireDefault(_SelectedUser);

var _Preload = require('./Preload');

var _Preload2 = _interopRequireDefault(_Preload);

var _User = require('./User');

var _User2 = _interopRequireDefault(_User);

// <tr key={index} onClick={this.selected.bind(this, index)}>
// 	<td>{el.id}</td>
// 	<td>{el.firstName}</td>
// 	<td>{el.lastName}</td>
// 	<td>{el.email}</td>
// 	<td>{el.phone}</td>
// </tr>

var UserList = (function (_Component) {
	_inherits(UserList, _Component);

	function UserList() {
		_classCallCheck(this, UserList);

		_get(Object.getPrototypeOf(UserList.prototype), 'constructor', this).call(this);
		this.state = {
			data: [],
			selectedUser: false,
			preload: true
		};
		this.loadData('http://www.filltext.com/?rows=1&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&adress={addressObject}&description={lorem|32}');
	}

	_createClass(UserList, [{
		key: 'loadData',
		value: function loadData(url) {
			var _this = this;

			this.preload = true;
			_axios2['default'].get(url).then(function (response, error) {
				_this.data = response.data;
				_this.setState({
					data: _this.data,
					preload: false
				});
			})['catch'](function (error) {
				alert(error);
			});
		}
	}, {
		key: 'filter',
		value: function filter(query) {
			query = query.toLowerCase();
			var items = this.data.filter(function (el) {
				var hasFirstName = el.firstName.toLowerCase().indexOf(query) !== -1,
				    hasLastName = el.lastName.toLowerCase().indexOf(query) !== -1,
				    hasId = el.id.toString().indexOf(query) !== -1;

				if (hasFirstName || hasLastName || hasId) {
					return el;
				}
			});

			this.setState({
				data: items
			});
		}
	}, {
		key: 'sort',
		value: function sort(el, isActive) {
			var selector = el;

			var sortItems = this.data.sort(function (a, b) {
				if (a[selector] < b[selector]) return -1;
				if (a[selector] > b[selector]) return 1;
				return 0;
			});

			isActive ? this.setState({ data: sortItems }) : this.setState({ data: sortItems.reverse() });
		}
	}, {
		key: 'selected',
		value: function selected(index) {
			this.setState({
				selectedUser: _react2['default'].createElement(_SelectedUser2['default'], {
					firstName: this.state.data[index].firstName,
					lastName: this.state.data[index].lastName,
					description: this.state.data[index].description,
					streetAddress: this.state.data[index].adress.streetAddress,
					city: this.state.data[index].adress.city,
					state: this.state.data[index].adress.state,
					zip: this.state.data[index].adress.zip
				})
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			return _react2['default'].createElement(
				'div',
				null,
				this.state.preload ? _react2['default'].createElement(_Preload2['default'], null) : null,
				_react2['default'].createElement(_Search2['default'], { filter: this.filter.bind(this) }),
				_react2['default'].createElement(
					'table',
					{ className: 'table table-hover table-bordered' },
					_react2['default'].createElement(
						'thead',
						null,
						_react2['default'].createElement(
							'tr',
							{ className: 'table__thead-row' },
							_react2['default'].createElement(_SortItem2['default'], {
								title: 'id',
								sort: this.sort.bind(this),
								sortTitle: 'id'
							}),
							_react2['default'].createElement(_SortItem2['default'], {
								title: 'First name',
								sort: this.sort.bind(this),
								sortTitle: 'firstName'
							}),
							_react2['default'].createElement(_SortItem2['default'], {
								title: 'Last name',
								sort: this.sort.bind(this),
								sortTitle: 'lastName'
							}),
							_react2['default'].createElement(
								'th',
								null,
								'email'
							),
							_react2['default'].createElement(
								'th',
								null,
								'phone'
							)
						)
					),
					_react2['default'].createElement(
						'tbody',
						null,
						this.state.data.map(function (el, index) {
							return _react2['default'].createElement(_User2['default'], {
								'function': _this2.selected.bind(_this2, index),
								key: index,
								id: el.id,
								firstName: el.firstName,
								lastName: el.lastName,
								email: el.email,
								phone: el.phone
							});
						})
					)
				),
				this.state.selectedUser
			);
		}
	}]);

	return UserList;
})(_react.Component);

exports['default'] = UserList;
module.exports = exports['default'];

//# sourceMappingURL=UserList-compiled.js.map